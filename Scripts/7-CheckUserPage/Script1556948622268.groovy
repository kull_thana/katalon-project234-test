import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.83.97.97:8081/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/h2_Products'), 'Products')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart 2'))

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/a_Carts            1'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/h2_Shopping Cart'), 'Shopping Cart')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/td_Papaya'), 'Papaya')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Papaya_amount'), '5')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/td_Garden'), 'Garden')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Garden_amount'), '2')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_confirm'))

WebUI.acceptAlert()

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Logout'))

